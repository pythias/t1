package t1

import "fmt"

type T1 struct {
	Name string
}

func (t1 *T1) Hello() {
	fmt.Printf("Hello, %s. from @v1.0.1", t1.Name)
}
